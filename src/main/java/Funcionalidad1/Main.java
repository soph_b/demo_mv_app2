package Funcionalidad1;

import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class Main {

    public static String csrf(){
        JSONObject csrf = Unirest.get("https://www.bolsadesantiago.com/api/Securities/csrfToken")
                .asJson()
                .getBody()
                .getObject();
        return csrf.get("csrf").toString();
    }

    public static String uf_recover(String start, String end){
        JSONObject uf_value = Unirest.post("https://www.bolsadesantiago.com/api/Comunes/getValorUF")
                .header("X-CSRF-Token",csrf())
                .header("Content-Type", "application/json")
                .body("{\"fechafin\":\""+end+"\",\"fechaini\":\""+start+"\"}")
                .asJson()
                .getBody()
                .getObject();
        return uf_value.get("listaResult").toString();
    }

    public static void main(String[] args) {
        System.out.println(uf_recover("2020-11-13","2020-11-13"));
    }
}
